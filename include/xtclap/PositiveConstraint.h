

/****************************************************************************** 
 * 
 *  file:  PositiveConstraint.h
 * 
 *  Copyright (c) 2014, Nicolas Martin
 *  All rights reverved.
 * 
 *  See the file COPYING in the top directory of this distribution for
 *  more information.
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef TCLAP_POSITIVECONSTRAINT_H
#define TCLAP_POSITIVECONSTRAINT_H

#include <string>
#include <vector>
#include <xtclap/BoundConstraint.h>

namespace TCLAP {

/**
 * A Constraint that constrains the Arg to only those values specified
 * in the constraint.
 */
template<class T>
class PositiveConstraint : public BoundConstraint<T>
{

	public:

		/**
		 * Constructor. 
		 */
		PositiveConstraint();
};

template<class T>
PositiveConstraint<T>::PositiveConstraint() :
    BoundConstraint<T>(0, std::numeric_limits<T>::max(), false, true) {}

} //namespace TCLAP
#endif 

