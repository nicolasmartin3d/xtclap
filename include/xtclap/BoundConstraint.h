

/****************************************************************************** 
 * 
 *  file:  BoundConstraint.h
 * 
 *  Copyright (c) 2014, Nicolas Martin
 *  All rights reverved.
 * 
 *  See the file COPYING in the top directory of this distribution for
 *  more information.
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef TCLAP_BOUNDCONSTRAINT_H
#define TCLAP_BOUNDCONSTRAINT_H

#include <string>
#include <vector>
#include <limits>
#include <xtclap/Constraint.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#else
#define HAVE_SSTREAM
#endif

#if defined(HAVE_SSTREAM)
#include <sstream>
#elif defined(HAVE_STRSTREAM)
#include <strstream>
#else
#error "Need a stringstream (sstream or strstream) to compile!"
#endif

namespace TCLAP {

/**
 * A Constraint that constrains the Arg to only those values specified
 * in the constraint.
 */
template<class T>
class BoundConstraint : public Constraint<T>
{

	public:

		/**
		 * Constructor. 
		 * \param min - minimum allowed value.
		 * \param max - maximum allowed value. 
		 * \param inclMin - if minimum value is inclusive (makes sense only for integral types). 
		 * \param inclMax - if maximum value is inclusive (makes sense only for integral types). 
		 */
		BoundConstraint(const T& min, const T& max =
                std::numeric_limits<T>::max(),
                bool inclMin = true, bool inclMax = true);

		/**
		 * Virtual destructor.
		 */
		virtual ~BoundConstraint() {}

		/**
		 * Returns a description of the Constraint. 
		 */
		virtual std::string description() const;

		/**
		 * Returns the short ID for the Constraint.
		 */
		virtual std::string shortID() const;

		/**
		 * The method used to verify that the value parsed from the command
		 * line meets the constraint.
		 * \param value - The value that will be checked. 
		 */
		virtual bool check(const T& value) const;
	
	protected:

		std::vector<T> _allowed;
        const T& m_min;
        const T& m_max;
        bool m_inclMin, m_inclMax;

		/**
		 * The string used to describe the allowed values of this constraint.
		 */
		std::string _typeDesc;

};

template<class T>
BoundConstraint<T>::BoundConstraint(
        const T& min, const T& max,
        bool inclMin, bool inclMax) :
    m_min(min), m_max(max),
    m_inclMin(inclMin),
    m_inclMax(inclMax)
{
#if defined(HAVE_SSTREAM)
    std::ostringstream os;
#elif defined(HAVE_STRSTREAM)
    std::ostrstream os;
#else
#error "Need a stringstream (sstream or strstream) to compile!"
#endif

    os << (m_inclMin ? "[" : "(") << min << "," << max <<
        (m_inclMax ? "]" : ")");
    _typeDesc = os.str();
}


template<class T>
bool BoundConstraint<T>::check( const T& val ) const
{
    return (m_inclMin ? m_min <= val : m_min < val) &&
        (m_inclMax ? m_max >= val : m_max > val);
}

template<class T>
std::string BoundConstraint<T>::shortID() const
{
    return _typeDesc;	
}

template<class T>
std::string BoundConstraint<T>::description() const
{
    return _typeDesc;	
}


} //namespace TCLAP
#endif 

